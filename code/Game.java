import java.util.Scanner;


public class Game {

	Grid grid;
	Robot[] allRobots;
	
	
	public Game()
	{
		createGrid();
		createRobots();
		calculateRobotMoves();
	}
	
	
	
	public void createGrid()
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please insert a grid upper-right limit");
		String limit = scan.nextLine();
		
		grid = new Grid(limit);
		
	}
	
	
	
	public void createRobots()
	{
		Scanner scanN = new Scanner(System.in);
		Scanner scanS = new Scanner(System.in);
		
		System.out.println("Can you please tell me how many robots you would like to create?");
		int robotNumb = scanN.nextInt();
		
		allRobots = new Robot[robotNumb];
		
		for (int i = 0; i < robotNumb; i++) 
		{
			System.out.println("Robot n." + (i+1));
			String lineOne = scanS.nextLine();
			String[] theXtheYtheOr = lineOne.split(" ");
			String lineTwo = scanS.nextLine();
			
			if(theXtheYtheOr.length==3)
			{
				try 
				{
					allRobots[i] = new Robot(Integer.parseInt(theXtheYtheOr[0]),Integer.parseInt(theXtheYtheOr[1]), theXtheYtheOr[2], lineTwo);
				}
				catch(NumberFormatException e)
				{
					System.out.println("Sorry, but what you input for the coordinates in not a number. :/");
				}
			}
			else
			{
				System.out.println("there is a problem with the coordinates of this Robot. \nDid you input the X, the Y and then Orientation or something else?");
			}
			
			
			System.out.println();
		}
		
		scanN.close();
		scanS.close();
	}
	
	
	public void calculateRobotMoves()
	{
		for(int x=0; x<allRobots.length; x++)		
		{
			
			Boolean isLost = false;
			
		try 
		{
				for(int q=0; q<allRobots[x].getInstr().length(); q++)
				{
					
					if(allRobots[x].returnX()<= grid.returnX()  && allRobots[x].returnY() <= grid.returnY())
					{
						char c = allRobots[x].getInstr().charAt(q);
						allRobots[x].moveRobot(c);
						
					}
					else
					{
						allRobots[x].moveToLastValidPosition();
						isLost = true;
						break;
					}
					
				}	

				if(isLost == false)
				{
					System.out.println(allRobots[x].getFinalPosition());
				}
				else
				{
					System.out.println(allRobots[x].getFinalPosition() + " LOST");
					isLost = false;
				}
		}
		catch(NumberFormatException e)
		{
			System.out.println("Robot n. " + (x+1) + " skipped, due to wrong information " );
		}
			
		
		}
	}
}
