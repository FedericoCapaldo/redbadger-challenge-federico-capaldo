
public class Robot {
	int x =0;
	int y =0;
	String orientation="N";
	String instructions ="";
	
	
	public Robot(int x, int y, String orientation, String instructions)
	{
		this.x = x;
		this.y = y;
		this.orientation = orientation.toUpperCase();
		this.instructions = instructions.toUpperCase();
	}
	
	public String getInstr()
	{
		return instructions;
	}
	
	
	public void moveRobot(char c)
	{
		
		if(c == 'R')
		{
			switch (orientation) {
			case "N":
			{
				orientation = "E";
				break;
			}
			case "S":
			{
				orientation = "W";
				break;
			}
			case "E":
			{
				orientation = "S";
				break;
			}
			case "W":
			{
				orientation = "N";
				break;
			}
			default:
			{	
				System.out.println("undefined Orientation.");
				break;
			}
			}
		}
		else if(c == 'L')
		{
			switch (orientation) {
			case "N":
			{
				orientation = "W";
				break;
			}
			case "S":
			{
				orientation = "E";
				break;
			}
			case "E":
			{
				orientation = "N";
				break;
			}
			case "W":
			{
				orientation = "S";
				break;
			}
			default:
			{
				System.out.println("undefined Orientation.");
				break;
			}
			}
		}
		else if(c == 'F')
		{
			switch (orientation) {
			case "N":
			{
				this.y += 1;
				break;
			}
			case "S":
			{
				this.y -= 1;
				break;
			}
			case "E":
			{
				this.x += 1;
				break;
			}
			case "W":
			{
				this.x -= 1;
				break;
			}
			default:
				System.out.println("undefined Orientation.");
				break;
			}
		}
		else
		{
			System.out.println("undefined command ignored.");
		}
	}
	
	
	public void moveToLastValidPosition()
	{
		
		switch (orientation) {
		case "N":
		{
			this.y -= 1;
			break;
		}
		case "S":
		{
			this.y += 1;
			break;
		}
		case "E":
		{
			this.x -= 1;
			break;
		}
		case "W":
		{
			this.x += 1;
			break;
		}
		default:
			break;
		}
		
		
	}
	
	
	public String getFinalPosition()
	{
		return "FINAL POSITION: " + x + " " + y + " " + orientation;
	}
	
	public int returnX()
	{
		return this.x;
	}
	
	public int returnY()
	{
		return this.y;
	}

}
