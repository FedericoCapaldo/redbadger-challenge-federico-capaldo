
public class Grid {

	int grid[][];
	int x = 0;
	int y = 0; 
	
	//(neat) constructor
	public Grid(String limit)
	{
		getCoordinateLimits(limit);
	}

	
	//convert the value from the users and initialises the grid
	public void getCoordinateLimits(String limit)
	{
		
		String[] twoCoordianre = limit.split(" ");
				
		if(twoCoordianre.length==2)
		{
			try
			{
				x = Integer.parseInt(twoCoordianre[0]);
				y = Integer.parseInt(twoCoordianre[1]);
				
				if(x<=50 && y<=50)
				{
					grid = new int[x][y];
				}
				else
				{
					System.out.println("mmh.. it looks like your grid is too big!");
					System.exit(1);
				}
				
			}
			catch(NumberFormatException e)
			{
				System.out.println("the coordinare for the limit are not in the right format");
			}
		}
		else
		{
			System.out.println("the coordinare for the limit are not in the right format");
			System.exit(1);
		}
			
	}
	
	public int returnX()
	{
		return this.x;
	}
	
	public int returnY()
	{
		return this.y;
	}
	
}
